import 'dart:async';

import 'package:challenge_nj/features/main/presentation/main_page.dart';
import 'package:challenge_nj/features/main/presentation/pages/select_language.dart';
import 'package:flutter/material.dart';
import 'package:simple_progress_indicators/simple_progress_indicators.dart';

import '../../features/home/presentation/home.dart';
import '../../features/network_dictionary/presentation/network_dictionary_page.dart';

class SplashScreen extends StatefulWidget {
  static Route route() => MaterialPageRoute(
        builder: (context) => const SplashScreen(),
      );

  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(
      const Duration(seconds: 3),
      () => Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (BuildContext context) => SelectLanguage(),
        ),
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Image(
            image: AssetImage("images/splash.png"),
          ),
          ProgressBarAnimation(
            duration: Duration(seconds: 3),
            backgroundColor: Colors.cyan,
            gradient: LinearGradient(
              colors: [
                Colors.yellowAccent,
                Colors.deepOrangeAccent,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          )
        ],
      )),
    );
  }
}
