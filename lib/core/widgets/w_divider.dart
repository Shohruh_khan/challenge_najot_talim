import 'package:flutter/material.dart';
import 'package:challenge_nj/assets/colors/colors.dart';

class WDivider extends StatelessWidget {
  final double height;
  final Color color;
  final EdgeInsets margin;
  const WDivider(
      {super.key,
      this.height = 1,
      this.color = Colors.grey,
      this.margin = EdgeInsets.zero});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: margin,
      height: height,
      color: color,
    );
  }
}
