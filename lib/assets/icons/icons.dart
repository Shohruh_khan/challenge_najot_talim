abstract class AppIcons {
  AppIcons._();

  /////////////////   Global Icons   ////////////////////////////
  ///
  ///
  ///
  static const homeActive = 'assets/icons/global/home.svg';
  static const homeInactive = 'assets/icons/global/home_inactive.svg';
  static const postActive = 'assets/icons/global/post_active.svg';
  static const postInactive = 'assets/icons/global/post_inactive.svg';
  static const createActive = 'assets/icons/global/create_active.svg';
  static const createInactive = 'assets/icons/global/create_inactive.svg';
  static const chatActive = 'assets/icons/global/chat_active.svg';
  static const chatInactive = 'assets/icons/global/chat_inactive.svg';
  static const serviceActive = 'assets/icons/global/service_active.svg';
  static const serviceInactive = 'assets/icons/global/service_inactive.svg';
  static const success = 'assets/icons/global/success.svg';
  static const error = 'assets/icons/global/error.svg';
  static const eye = 'assets/icons/global/eye.svg';
  static const validationError = 'assets/icons/global/validation_error.svg';
  static const arrowBack = 'assets/icons/global/arrow_back.svg';
  static const search = 'assets/icons/global/search_text_field.svg';
  static const close = 'assets/icons/global/close_text_field.svg';
  static const back = 'assets/icons/global/arrow_back.svg';
  /////////////////   Global Icons End   ////////////////////////
  ///

}
