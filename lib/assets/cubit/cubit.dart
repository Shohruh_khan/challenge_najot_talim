// import 'package:bloc/bloc.dart';
// import 'package:challenge_nj/assets/cubit/states.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:sqflite/sqflite.dart';
//
// class DictionaryCubit extends Cubit<DictionaryStates> {
//   DictionaryCubit() : super(InitialDictionaryState());
//
//   static DictionaryCubit get(context) => BlocProvider.of(context);
//
//   Database? database;
//
//   void createDatabase() {
//     openDatabase(
//       'assets/eng_dictionary.db',
//       version: 1,
//       onOpen: (database) {
//         print('database file opened');
//       },
//     ).then((value) {
//       database = value;
//       emit(OpenDictionaryDatabaseState());
//     }).catchError((error) {
//       print('error while opening database');
//     });
//   }
//
//   void insertDatabase() {
//     database!.transaction((txn) async {
//       txn.rawInsert('INSERT INTO definition'
//           '( WORD,DESCRIPTION) VALUES'
//           '("fofo","very good")');
//     }).then((value) {
//       print(value);
//       emit(InsertingIntoDictionaryDatabaseState());
//     }).catchError((e) {
//       print('an error while inserting');
//     });
//   }
// }
