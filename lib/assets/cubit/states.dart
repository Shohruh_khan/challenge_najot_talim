

abstract class DictionaryStates{}

class InitialDictionaryState extends DictionaryStates{}
class OpenDictionaryDatabaseState extends DictionaryStates{}
class InsertingIntoDictionaryDatabaseState extends DictionaryStates{}