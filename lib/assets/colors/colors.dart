import 'package:flutter/cupertino.dart';

// const primary = _dodgerBlue;
const white = _white;
const background = _whiteAccent;
const stroke = _solitude;
const secondary = _heather;
const grey = _c494646;
const dark = _prussianBlue;
const red = _cD64751;
const black = _black;
const blue = _cornflowerBlue;
const greyBlue = _echoBlue;
const darkTextColor = _licorice;
const scaffoldBackground = _aliceBlue;
const photos = _cFED5D8;
const videos = _cD0D4F9;
const music = _cC0EADE;
const documents = _cFFDFC0;
const downloads = _cECC8FD;
const greyTextColor = _c989DAD;
const blueTextColor = _c50A8EA;
const navigationBarShadowColor = _c525F68;
const textFieldFillColor = _cDFE7ED;
const lightGrey = _c5C5C5C;
const lightBlue = _c84C4F4;
const backgroundCheckbox = _cFAFBFF;
const deepDarkTextColor = _c171B28;
const lighterGrey = _cEBF1F5;
const bodyText1Color = _c111111;
const brown = _cBF7C2E;
const primary = _cF5BC41;
const accentGrey = _c1C2E45;
const lightBrown = _cDECDB8;
const arrowGrey = _c606060;

/// Login Screen Colors
const inActiveLoginButton = _cD2A36D;
const activeLoginButton = _goldenPoppy;
const inactiveTextColor = _manatee;
const activeTextColor = _baliHai;
const hintTextColor = _9199AA;
const signUpScreenAppBarTitle = _c757575;
const backgroundTextFieldColor = _cEFEFEF;

const choosePicTitle = _c382823;
const borderColor = _cE9EEF3;
const titleTextColor = _c263238;
const timerColor = _c0062FF;
const searchBackgroundColor = _cEDEFF2;
const searchBorderColor = _cE6E7EC;
const verificationHintColor = _c5B5B5B;

const darkThemeSvg = _cD7DCE0;
const prayTimesContainer = _lavender;
const lightGreyText = _manatee;
const yellowShadow = _sunFlower;
const lightRed = _valencia;
const green = _c019147;
const silver = _silver;
const mortarBorderColor = _mortar;

const dividerColor = _cBDBDBD;

const baliHai = _baliHai;

// Link: http://www.color-blindness.com/color-name-hue/

const _white = Color(0XFFFFFFFF);
const _whiteAccent = Color(0XFFFAFAFA);
const _solitude = Color(0XFFECEDF0);
const _heather = Color(0XFFAFB6C4);
const _prussianBlue = Color(0XFF001444);
const _black = Color(0XFF000000);
const _echoBlue = Color(0xFFA9BDCB);
const _cornflowerBlue = Color(0XFF0535DC);
const _licorice = Color(0XFF2C3246);
const _aliceBlue = Color(0XFFFCFDFF);
const _darkThemeSvg = _cD7DCE0;
const _manatee = Color(0XFF929BA5);
const _sunFlower = Color(0XFFE7B815);
const _valencia = Color(0XFFD84E5F);
const _lavender = Color(0XFFE2EAF9);
const _baliHai = Color(0XFF8C97AB);
const _silver = Color(0XFFBBBBBB);
const _mortar = Color(0XFF585858);

const _confetti = Color(0XFFE1BE44);
const _goldenPoppy = Color(0XFFEAB500);
//const _manatee = Color(0XFF8C94A5);
const darkPastelGreen = Color(0XFF00CE3A);

// Unnamed colors
const _9199AA = Color(0XFF9199AA);
const _cBDBDBD = Color(0XFFBDBDBD);
const _cFED5D8 = Color(0XFFFED5D8);
const _cD0D4F9 = Color(0XFFD0D4F9);
const _cC0EADE = Color(0XFFC0EADE);
const _cFFDFC0 = Color(0XFFFFDFC0);
const _cECC8FD = Color(0XFFECC8FD);
const _c989DAD = Color(0XFF989DAD);
const _c50A8EA = Color(0XFF50A8EA);
const _c525F68 = Color(0XFF525F68);
const _cD2A36D = Color(0XFFD2A36D);
const _cDFE7ED = Color(0XFFDFE7ED);
const _c84C4F4 = Color(0XFF84C4F4);
const _cFAFBFF = Color(0XFFFAFBFF);
const _c171B28 = Color(0XFF171B28);
const _cEBF1F5 = Color(0XFFEBF1F5);
const _cD7DCE0 = Color(0XFFD7DCE0);
const _c494646 = Color(0XFF494646);
const _c5C5C5C = Color(0XFF5C5C5C);
const _cD64751 = Color(0XFFD64751);
const _c111111 = Color(0XFF111111);
const _cF5BC41 = Color(0XFFF5BC41);
const _cBF7C2E = Color(0XFFBF7C2E);
const _c1C2E45 = Color(0XFF1C2E45);
const _cDECDB8 = Color(0XFFDECDB8);
const _c606060 = Color(0XFF606060);
const _c019147 = Color(0XFF019147);
const _c757575 = Color(0XFF757575);
const _cEFEFEF = Color(0XFFEFEFEF);
const _c5B5B5B = Color(0XFF5B5B5B);
const _c382823 = Color(0XFF382823);
const _cE9EEF3 = Color(0XFFE9EEF3);
const _c263238 = Color(0XFF263238);
const _c0062FF = Color(0XFF0062FF);
const _cE6E7EC = Color(0XFFE6E7EC);
const _cEDEFF2 = Color(0XFFEDEFF2);
