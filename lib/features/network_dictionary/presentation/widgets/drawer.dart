import 'package:challenge_nj/features/create_pdf/presentaion/bloc/pdf_bloc.dart';
import 'package:challenge_nj/features/create_pdf/presentaion/create_pdf_page.dart';
import 'package:challenge_nj/features/main/presentation/bloc/local_dictionary_bloc.dart';
import 'package:challenge_nj/features/main/presentation/pages/definition.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/data/service_locator.dart';
import '../../../../core/widgets/w_scale.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({super.key});

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

bool isEnglish = true;

class _AppDrawerState extends State<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Container(
            height: 140,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
              Navigator.pushNamed(context, '/locationPage');
            },
            child: const ListTile(
              leading: Icon(Icons.language),
              title: Text(
                'Definition from api',
                style: TextStyle(),
              ),
            ),
          ),
          BlocBuilder<LocalDictionaryBloc, LocalDictionaryState>(
            builder: (context, state) {
              return ListTile(
                title: const Text('Eng_Uzb'),
                trailing: Switch(
                  value: isEnglish,
                  onChanged: (value) {
                    Navigator.of(context).pop();
                    setState(() {
                      isEnglish = !isEnglish;
                    });
                    print("${state.isEngToUzb}");

                    context
                        .read<LocalDictionaryBloc>()
                        .add(LocalDictionaryEvent.init(isEnglish: isEnglish));
                  },
                ),
              );
            },
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BlocProvider.value(
                      value: sl<PdfBloc>(),
                      child: const CreatePdfPage(),
                    ),
                  ));
            },
            child: const ListTile(
              leading: Icon(Icons.picture_as_pdf),
              title: Text(
                'Create pdf',
                style: TextStyle(),
              ),
            ),
          ),
          BlocBuilder<LocalDictionaryBloc, LocalDictionaryState>(
            builder: (context, state) {
              return WScaleAnimation(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const DefinitionPage()));
                },
                child: const ListTile(
                  title: Text('Definition'),
                  leading: Icon(Icons.wordpress_sharp),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
