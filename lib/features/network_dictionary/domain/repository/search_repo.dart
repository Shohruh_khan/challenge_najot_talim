import 'package:challenge_nj/core/data/either.dart';
import 'package:challenge_nj/core/error/failure.dart';
import 'package:challenge_nj/features/network_dictionary/domain/entities/word.dart';

abstract class SearchRepository {
  Future<Either<Failure, WordEntity>> getDefinition(String query);
}
