import 'package:equatable/equatable.dart';

class EngUzbWordEntities with EquatableMixin {
  final String eng;
  final String uzb;
  final bool isFav;
  final bool isHistory;

  EngUzbWordEntities(this.eng, this.uzb, this.isFav, this.isHistory);
  @override
  List<Object?> get props => [eng, uzb, isFav, isHistory];
}
