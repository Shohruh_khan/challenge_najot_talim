// import '../../../../core/data/either.dart';
// import '../../../../core/error/failure.dart';
// import '../../../../core/usecases/usecase.dart';
// import '../../data/models/adapter.dart';
// import '../../data/models/dictionary_model.dart';
// import '../entities/eng_uz_dictionary_entity.dart';
// import '../repository/eng_uzb_word_repository.dart';
//
//
//
// // class GetEngUzbWordsUseCase {
// //   final EngDictionaryRepository _repository;
// //
// //   GetEngUzbWordsUseCase(this._repository);
// //
// //   // Future<List<EngUzbWord>> getEngUzbWords() async {
// //   //   final entities = await _repository.getEngUzbWords(eng: '', uzb: '', isFav: true, isHistory: true);
// //   //   return EngDictionaryMapper.fromEntityList(entities);
// //   // }
// //   Future<Either<String, List<EngUzbWord>>> call(
// //       EngUzbWordEntities engUzbWordEntities) async {
// //     try {
// //       final engUzbWords = await _repository.getEngUzbWords(
// //           eng: '', uzb: '', isFav: true, isHistory: true);
// //       return Right(EngDictionaryMapper.fromEntityList(engUzbWords));
// //     } catch (e) {
// //       return Left(e.toString());
// //     }
// //   }
// // }
//
// class GetEngUzbWordsUseCase
//     implements UseCase<List<EngUzbWordEntities>, GetWordsParams> {
//   final EngDictionaryRepository _repository;
//
//   GetEngUzbWordsUseCase({required EngDictionaryRepository repository})
//       : _repository = repository;
//   @override
//   Future<Either<Failure, List<EngUzbWordEntities>>> call(params) {
//     return _repository.getEngUzbWords(eng: eng, uzb: uzb, isFav: isFav, isHistory: isHistory)
//   }
// }
// class GetWordsParams {
//   final String eng;
//   final String uzb;
//   final bool isFav;
//   final bool isHistory;
//
//   GetWordsParams(this.eng, this.uzb, this.isFav, this.isHistory);
// }
