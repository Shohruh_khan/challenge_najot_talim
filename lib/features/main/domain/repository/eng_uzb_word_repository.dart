import 'package:challenge_nj/features/main/domain/entities/eng_uz_dictionary_entity.dart';

import '../../data/models/dictionary_model.dart';

abstract class EngDictionaryRepository {
  Future<List<EngUzbWordEntities>> getEngUzbWords({
    required String eng,
    required String uzb,
    required bool isFav,
    required bool isHistory,
  });
}
