import 'package:flutter/material.dart';

class WordListWidget extends StatelessWidget {
  final List<Map<String, dynamic>> words;

  const WordListWidget(this.words);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: words.length,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          title: Text(words[index]['english']),
          subtitle: Text(words[index]['uzbek']),
        );
      },
    );
  }
}
