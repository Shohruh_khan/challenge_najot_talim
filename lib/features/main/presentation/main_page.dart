import 'package:challenge_nj/core/widgets/w_scale.dart';
import 'package:challenge_nj/features/main/presentation/pages/show_dictionary.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../../network_dictionary/presentation/widgets/drawer.dart';
import '../data/models/dictionary_model.dart';
import 'bloc/local_dictionary_bloc.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<EngUzbWord> _englishWords = [];
  List<EngUzbWord> _uzbWords = [];
  late bool isEnglish = true;

  @override
  void initState() {
    super.initState();
    _loadUzbtoEng();
    _loadEngtoUzb();
  }

  Future<void> _loadEngtoUzb() async {
    final db = await openDatabase(
      join(await getDatabasesPath(), 'eng_dictionary.db'),
      version: 1,
    );
    final List<Map<String, dynamic>> maps = await db.query('eng_uzb');

    setState(() {
      _englishWords = List.generate(maps.length, (i) {
        return EngUzbWord(
          maps[i]['eng'],
          maps[i]['uzb'],
        );
      });
    });
  }

  Future<void> _loadUzbtoEng() async {
    final db = await openDatabase(
      join(await getDatabasesPath(), 'eng_dictionary.db'),
      version: 1,
    );
    final List<Map<String, dynamic>> maps = await db.query('uzb_eng');

    setState(() {
      _uzbWords = List.generate(maps.length, (i) {
        return EngUzbWord(
          maps[i]['eng'],
          maps[i]['uzb'],
        );
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: const AppDrawer(),
        appBar: AppBar(
          title: const Text('Eng-Uzb Words'),
          actions: [
            BlocBuilder<LocalDictionaryBloc, LocalDictionaryState>(
              builder: (context, state) {
                return WScaleAnimation(
                    onTap: () {
                      setState(() {
                        isEnglish = !isEnglish;
                      });
                      print("${state.isEngToUzb}");

                      context
                          .read<LocalDictionaryBloc>()
                          .add(LocalDictionaryEvent.init(isEnglish: isEnglish));
                    },
                    child: const Icon(Icons.change_circle_sharp));
              },
            )
          ],
        ),
        body: BlocBuilder<LocalDictionaryBloc, LocalDictionaryState>(
          builder: (context, state) {
            return ListView.builder(
              itemCount:
                  state.isEngToUzb ? _englishWords.length : _uzbWords.length,
              itemBuilder: (BuildContext context, int index) {
                final englishWord = _englishWords[index];
                final uzbWord = _uzbWords[index];
                return GestureDetector(
                  onLongPress: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(
                              state.isEngToUzb ? englishWord.eng : uzbWord.uzb),
                          content: Text(
                              state.isEngToUzb ? englishWord.uzb : uzbWord.eng),
                          actions: [
                            WScaleAnimation(
                              child: const Text('OK'),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ShowDictionary(
                          title: state.isEngToUzb
                              ? _englishWords[index].eng
                              : _uzbWords[index].uzb,
                          description: state.isEngToUzb
                              ? _englishWords[index].uzb
                              : _uzbWords[index].eng,
                        ),
                      ),
                    );
                  },
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            state.isEngToUzb ? englishWord.eng : uzbWord.uzb,
                            style: const TextStyle(
                                fontSize: 28, fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
