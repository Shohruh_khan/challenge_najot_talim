// import 'package:freezed_annotation/freezed_annotation.dart';

// import '../../../../core/models/formz/formz_status.dart';
// import '../../domain/entities/eng_uz_dictionary_entity.dart';
// part of 'dictionary_bloc.dart';




// @Freezed()
// class DictionaryState with _$DictionaryState {
//   const factory DictionaryState({
//     @Default(FormzStatus.pure) FormzStatus status,
//     @Default(FormzStatus.pure) FormzStatus helperStatus,
//     @Default([]) List<EngUzbWordEntities> words,
//     @Default([]) List<EngUzbWordEntities> results,
//     @Default(null) EngUzbWordEntities? selectedWord,
//     @Default(0) int currentPage,
//     @Default(0) int totalPages,
//   }) = _DictionaryState;
// }
