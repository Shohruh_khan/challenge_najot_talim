import 'package:challenge_nj/core/widgets/w_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../assets/colors/colors.dart';
import '../bloc/local_dictionary_bloc.dart';
import '../main_page.dart';

class SelectLanguage extends StatelessWidget {
  SelectLanguage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            WButton(
                text: 'English',
                textStyle: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                  color: white,
                ),
                color: blue,
                onTap: () {
                  context
                      .read<LocalDictionaryBloc>()
                      .add(const LocalDictionaryEvent.init(isEnglish: true));
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const MainPage()));
                }),
            const SizedBox(height: 40),
            WButton(
                text: "O'zbekcha",
                textStyle: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                  color: white,
                ),
                color: green,
                onTap: () {
                  context
                      .read<LocalDictionaryBloc>()
                      .add(const LocalDictionaryEvent.init(isEnglish: false));
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const MainPage()));
                }),
          ],
        ),
      ),
    );
  }
}
