import 'dart:async';

import 'package:challenge_nj/assets/icons/icons.dart';
import 'package:challenge_nj/core/widgets/w_scale.dart';
import 'package:challenge_nj/features/main/data/models/definition.dart';
import 'package:challenge_nj/features/main/presentation/pages/show_dictionary.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../bloc/local_dictionary_bloc.dart';

class DefinitionPage extends StatefulWidget {
  const DefinitionPage({super.key});

  @override
  _DefinitionPageState createState() => _DefinitionPageState();
}

class _DefinitionPageState extends State<DefinitionPage> {
  List<DefinitionModel> _definition = [];

  late bool isEnglish = true;

  @override
  void initState() {
    super.initState();
    _loadDefinition();
  }

  Future<void> _loadDefinition() async {
    final db = await openDatabase(
      join(await getDatabasesPath(), 'eng_dictionary.db'),
      version: 1,
    );
    final List<Map<String, dynamic>> maps = await db.query('definition');

    setState(() {
      _definition = List.generate(maps.length, (i) {
        return DefinitionModel(
          maps[i]['Word'],
          maps[i]['Description'],
        );
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: WScaleAnimation(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SvgPicture.asset(AppIcons.back),
            ),
          ),
          title: const Text('Definition Screen'),
        ),
        body: BlocBuilder<LocalDictionaryBloc, LocalDictionaryState>(
          builder: (context, state) {
            return ListView.builder(
              itemCount: _definition.length,
              itemBuilder: (BuildContext context, int index) {
                final definition = _definition[index];
                return GestureDetector(
                  onLongPress: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(definition.Word),
                          content: Text(definition.Description),
                          actions: [
                            WScaleAnimation(
                              child: const Text('OK'),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ShowDictionary(
                            title: _definition[index].Word,
                            description: _definition[index].Description),
                      ),
                    );
                  },
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              definition.Description,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
