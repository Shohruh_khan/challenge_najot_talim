import 'package:challenge_nj/assets/icons/icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/widgets/w_scale.dart';
import '../bloc/local_dictionary_bloc.dart';

class ShowDictionary extends StatelessWidget {
  const ShowDictionary(
      {Key? key, required this.title, required this.description})
      : super(key: key);
  final String title;
  final String description;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: WScaleAnimation(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: SvgPicture.asset(AppIcons.back),
          ),
        ),
        title: const Text('Show Result'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          BlocBuilder<LocalDictionaryBloc, LocalDictionaryState>(
            builder: (context, state) {
              return Text(
                title,
                style:
                    const TextStyle(fontSize: 32, fontWeight: FontWeight.w600),
              );
            },
          ),
          const SizedBox(height: 18),
          BlocBuilder<LocalDictionaryBloc, LocalDictionaryState>(
            builder: (context, state) {
              return Text(
                description,
                style:
                    const TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
              );
            },
          ),
        ],
      ),
    );
  }
}
