// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'local_dictionary_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LocalDictionaryEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadEngUzb,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadUzbEng,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadDefinition,
    required TResult Function(bool isEnglish) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult? Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult? Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult? Function(bool isEnglish)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult Function(bool isEnglish)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadEngUzb value) loadEngUzb,
    required TResult Function(_LoadUzbEng value) loadUzbEng,
    required TResult Function(_LoadDefinition value) loadDefinition,
    required TResult Function(_Init value) init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_LoadEngUzb value)? loadEngUzb,
    TResult? Function(_LoadUzbEng value)? loadUzbEng,
    TResult? Function(_LoadDefinition value)? loadDefinition,
    TResult? Function(_Init value)? init,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadEngUzb value)? loadEngUzb,
    TResult Function(_LoadUzbEng value)? loadUzbEng,
    TResult Function(_LoadDefinition value)? loadDefinition,
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocalDictionaryEventCopyWith<$Res> {
  factory $LocalDictionaryEventCopyWith(LocalDictionaryEvent value,
          $Res Function(LocalDictionaryEvent) then) =
      _$LocalDictionaryEventCopyWithImpl<$Res, LocalDictionaryEvent>;
}

/// @nodoc
class _$LocalDictionaryEventCopyWithImpl<$Res,
        $Val extends LocalDictionaryEvent>
    implements $LocalDictionaryEventCopyWith<$Res> {
  _$LocalDictionaryEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_LoadEngUzbCopyWith<$Res> {
  factory _$$_LoadEngUzbCopyWith(
          _$_LoadEngUzb value, $Res Function(_$_LoadEngUzb) then) =
      __$$_LoadEngUzbCopyWithImpl<$Res>;
  @useResult
  $Res call({String query, ValueChanged<String> onFailure});
}

/// @nodoc
class __$$_LoadEngUzbCopyWithImpl<$Res>
    extends _$LocalDictionaryEventCopyWithImpl<$Res, _$_LoadEngUzb>
    implements _$$_LoadEngUzbCopyWith<$Res> {
  __$$_LoadEngUzbCopyWithImpl(
      _$_LoadEngUzb _value, $Res Function(_$_LoadEngUzb) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? query = null,
    Object? onFailure = null,
  }) {
    return _then(_$_LoadEngUzb(
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$_LoadEngUzb implements _LoadEngUzb {
  const _$_LoadEngUzb({required this.query, required this.onFailure});

  @override
  final String query;
  @override
  final ValueChanged<String> onFailure;

  @override
  String toString() {
    return 'LocalDictionaryEvent.loadEngUzb(query: $query, onFailure: $onFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoadEngUzb &&
            (identical(other.query, query) || other.query == query) &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, query, onFailure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoadEngUzbCopyWith<_$_LoadEngUzb> get copyWith =>
      __$$_LoadEngUzbCopyWithImpl<_$_LoadEngUzb>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadEngUzb,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadUzbEng,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadDefinition,
    required TResult Function(bool isEnglish) init,
  }) {
    return loadEngUzb(query, onFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult? Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult? Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult? Function(bool isEnglish)? init,
  }) {
    return loadEngUzb?.call(query, onFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult Function(bool isEnglish)? init,
    required TResult orElse(),
  }) {
    if (loadEngUzb != null) {
      return loadEngUzb(query, onFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadEngUzb value) loadEngUzb,
    required TResult Function(_LoadUzbEng value) loadUzbEng,
    required TResult Function(_LoadDefinition value) loadDefinition,
    required TResult Function(_Init value) init,
  }) {
    return loadEngUzb(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_LoadEngUzb value)? loadEngUzb,
    TResult? Function(_LoadUzbEng value)? loadUzbEng,
    TResult? Function(_LoadDefinition value)? loadDefinition,
    TResult? Function(_Init value)? init,
  }) {
    return loadEngUzb?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadEngUzb value)? loadEngUzb,
    TResult Function(_LoadUzbEng value)? loadUzbEng,
    TResult Function(_LoadDefinition value)? loadDefinition,
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (loadEngUzb != null) {
      return loadEngUzb(this);
    }
    return orElse();
  }
}

abstract class _LoadEngUzb implements LocalDictionaryEvent {
  const factory _LoadEngUzb(
      {required final String query,
      required final ValueChanged<String> onFailure}) = _$_LoadEngUzb;

  String get query;
  ValueChanged<String> get onFailure;
  @JsonKey(ignore: true)
  _$$_LoadEngUzbCopyWith<_$_LoadEngUzb> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_LoadUzbEngCopyWith<$Res> {
  factory _$$_LoadUzbEngCopyWith(
          _$_LoadUzbEng value, $Res Function(_$_LoadUzbEng) then) =
      __$$_LoadUzbEngCopyWithImpl<$Res>;
  @useResult
  $Res call({String query, ValueChanged<String> onFailure});
}

/// @nodoc
class __$$_LoadUzbEngCopyWithImpl<$Res>
    extends _$LocalDictionaryEventCopyWithImpl<$Res, _$_LoadUzbEng>
    implements _$$_LoadUzbEngCopyWith<$Res> {
  __$$_LoadUzbEngCopyWithImpl(
      _$_LoadUzbEng _value, $Res Function(_$_LoadUzbEng) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? query = null,
    Object? onFailure = null,
  }) {
    return _then(_$_LoadUzbEng(
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$_LoadUzbEng implements _LoadUzbEng {
  const _$_LoadUzbEng({required this.query, required this.onFailure});

  @override
  final String query;
  @override
  final ValueChanged<String> onFailure;

  @override
  String toString() {
    return 'LocalDictionaryEvent.loadUzbEng(query: $query, onFailure: $onFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoadUzbEng &&
            (identical(other.query, query) || other.query == query) &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, query, onFailure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoadUzbEngCopyWith<_$_LoadUzbEng> get copyWith =>
      __$$_LoadUzbEngCopyWithImpl<_$_LoadUzbEng>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadEngUzb,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadUzbEng,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadDefinition,
    required TResult Function(bool isEnglish) init,
  }) {
    return loadUzbEng(query, onFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult? Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult? Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult? Function(bool isEnglish)? init,
  }) {
    return loadUzbEng?.call(query, onFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult Function(bool isEnglish)? init,
    required TResult orElse(),
  }) {
    if (loadUzbEng != null) {
      return loadUzbEng(query, onFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadEngUzb value) loadEngUzb,
    required TResult Function(_LoadUzbEng value) loadUzbEng,
    required TResult Function(_LoadDefinition value) loadDefinition,
    required TResult Function(_Init value) init,
  }) {
    return loadUzbEng(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_LoadEngUzb value)? loadEngUzb,
    TResult? Function(_LoadUzbEng value)? loadUzbEng,
    TResult? Function(_LoadDefinition value)? loadDefinition,
    TResult? Function(_Init value)? init,
  }) {
    return loadUzbEng?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadEngUzb value)? loadEngUzb,
    TResult Function(_LoadUzbEng value)? loadUzbEng,
    TResult Function(_LoadDefinition value)? loadDefinition,
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (loadUzbEng != null) {
      return loadUzbEng(this);
    }
    return orElse();
  }
}

abstract class _LoadUzbEng implements LocalDictionaryEvent {
  const factory _LoadUzbEng(
      {required final String query,
      required final ValueChanged<String> onFailure}) = _$_LoadUzbEng;

  String get query;
  ValueChanged<String> get onFailure;
  @JsonKey(ignore: true)
  _$$_LoadUzbEngCopyWith<_$_LoadUzbEng> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_LoadDefinitionCopyWith<$Res> {
  factory _$$_LoadDefinitionCopyWith(
          _$_LoadDefinition value, $Res Function(_$_LoadDefinition) then) =
      __$$_LoadDefinitionCopyWithImpl<$Res>;
  @useResult
  $Res call({String query, ValueChanged<String> onFailure});
}

/// @nodoc
class __$$_LoadDefinitionCopyWithImpl<$Res>
    extends _$LocalDictionaryEventCopyWithImpl<$Res, _$_LoadDefinition>
    implements _$$_LoadDefinitionCopyWith<$Res> {
  __$$_LoadDefinitionCopyWithImpl(
      _$_LoadDefinition _value, $Res Function(_$_LoadDefinition) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? query = null,
    Object? onFailure = null,
  }) {
    return _then(_$_LoadDefinition(
      query: null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$_LoadDefinition implements _LoadDefinition {
  const _$_LoadDefinition({required this.query, required this.onFailure});

  @override
  final String query;
  @override
  final ValueChanged<String> onFailure;

  @override
  String toString() {
    return 'LocalDictionaryEvent.loadDefinition(query: $query, onFailure: $onFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoadDefinition &&
            (identical(other.query, query) || other.query == query) &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, query, onFailure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LoadDefinitionCopyWith<_$_LoadDefinition> get copyWith =>
      __$$_LoadDefinitionCopyWithImpl<_$_LoadDefinition>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadEngUzb,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadUzbEng,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadDefinition,
    required TResult Function(bool isEnglish) init,
  }) {
    return loadDefinition(query, onFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult? Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult? Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult? Function(bool isEnglish)? init,
  }) {
    return loadDefinition?.call(query, onFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult Function(bool isEnglish)? init,
    required TResult orElse(),
  }) {
    if (loadDefinition != null) {
      return loadDefinition(query, onFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadEngUzb value) loadEngUzb,
    required TResult Function(_LoadUzbEng value) loadUzbEng,
    required TResult Function(_LoadDefinition value) loadDefinition,
    required TResult Function(_Init value) init,
  }) {
    return loadDefinition(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_LoadEngUzb value)? loadEngUzb,
    TResult? Function(_LoadUzbEng value)? loadUzbEng,
    TResult? Function(_LoadDefinition value)? loadDefinition,
    TResult? Function(_Init value)? init,
  }) {
    return loadDefinition?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadEngUzb value)? loadEngUzb,
    TResult Function(_LoadUzbEng value)? loadUzbEng,
    TResult Function(_LoadDefinition value)? loadDefinition,
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (loadDefinition != null) {
      return loadDefinition(this);
    }
    return orElse();
  }
}

abstract class _LoadDefinition implements LocalDictionaryEvent {
  const factory _LoadDefinition(
      {required final String query,
      required final ValueChanged<String> onFailure}) = _$_LoadDefinition;

  String get query;
  ValueChanged<String> get onFailure;
  @JsonKey(ignore: true)
  _$$_LoadDefinitionCopyWith<_$_LoadDefinition> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_InitCopyWith<$Res> {
  factory _$$_InitCopyWith(_$_Init value, $Res Function(_$_Init) then) =
      __$$_InitCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isEnglish});
}

/// @nodoc
class __$$_InitCopyWithImpl<$Res>
    extends _$LocalDictionaryEventCopyWithImpl<$Res, _$_Init>
    implements _$$_InitCopyWith<$Res> {
  __$$_InitCopyWithImpl(_$_Init _value, $Res Function(_$_Init) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isEnglish = null,
  }) {
    return _then(_$_Init(
      isEnglish: null == isEnglish
          ? _value.isEnglish
          : isEnglish // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_Init implements _Init {
  const _$_Init({required this.isEnglish});

// required List<EngUzbWord> allWords,
  @override
  final bool isEnglish;

  @override
  String toString() {
    return 'LocalDictionaryEvent.init(isEnglish: $isEnglish)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Init &&
            (identical(other.isEnglish, isEnglish) ||
                other.isEnglish == isEnglish));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isEnglish);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InitCopyWith<_$_Init> get copyWith =>
      __$$_InitCopyWithImpl<_$_Init>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadEngUzb,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadUzbEng,
    required TResult Function(String query, ValueChanged<String> onFailure)
        loadDefinition,
    required TResult Function(bool isEnglish) init,
  }) {
    return init(isEnglish);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult? Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult? Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult? Function(bool isEnglish)? init,
  }) {
    return init?.call(isEnglish);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query, ValueChanged<String> onFailure)? loadEngUzb,
    TResult Function(String query, ValueChanged<String> onFailure)? loadUzbEng,
    TResult Function(String query, ValueChanged<String> onFailure)?
        loadDefinition,
    TResult Function(bool isEnglish)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(isEnglish);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadEngUzb value) loadEngUzb,
    required TResult Function(_LoadUzbEng value) loadUzbEng,
    required TResult Function(_LoadDefinition value) loadDefinition,
    required TResult Function(_Init value) init,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_LoadEngUzb value)? loadEngUzb,
    TResult? Function(_LoadUzbEng value)? loadUzbEng,
    TResult? Function(_LoadDefinition value)? loadDefinition,
    TResult? Function(_Init value)? init,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadEngUzb value)? loadEngUzb,
    TResult Function(_LoadUzbEng value)? loadUzbEng,
    TResult Function(_LoadDefinition value)? loadDefinition,
    TResult Function(_Init value)? init,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements LocalDictionaryEvent {
  const factory _Init({required final bool isEnglish}) = _$_Init;

// required List<EngUzbWord> allWords,
  bool get isEnglish;
  @JsonKey(ignore: true)
  _$$_InitCopyWith<_$_Init> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$LocalDictionaryState {
  FormzStatus get status => throw _privateConstructorUsedError;
  FormzStatus get helperStatus => throw _privateConstructorUsedError;
  List<EngUzbWord> get words => throw _privateConstructorUsedError;
  List<EngUzbWord> get results => throw _privateConstructorUsedError;
  EngUzbWordEntities? get selectedWord => throw _privateConstructorUsedError;
  bool get isEngToUzb => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LocalDictionaryStateCopyWith<LocalDictionaryState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocalDictionaryStateCopyWith<$Res> {
  factory $LocalDictionaryStateCopyWith(LocalDictionaryState value,
          $Res Function(LocalDictionaryState) then) =
      _$LocalDictionaryStateCopyWithImpl<$Res, LocalDictionaryState>;
  @useResult
  $Res call(
      {FormzStatus status,
      FormzStatus helperStatus,
      List<EngUzbWord> words,
      List<EngUzbWord> results,
      EngUzbWordEntities? selectedWord,
      bool isEngToUzb});
}

/// @nodoc
class _$LocalDictionaryStateCopyWithImpl<$Res,
        $Val extends LocalDictionaryState>
    implements $LocalDictionaryStateCopyWith<$Res> {
  _$LocalDictionaryStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? helperStatus = null,
    Object? words = null,
    Object? results = null,
    Object? selectedWord = freezed,
    Object? isEngToUzb = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      helperStatus: null == helperStatus
          ? _value.helperStatus
          : helperStatus // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      words: null == words
          ? _value.words
          : words // ignore: cast_nullable_to_non_nullable
              as List<EngUzbWord>,
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<EngUzbWord>,
      selectedWord: freezed == selectedWord
          ? _value.selectedWord
          : selectedWord // ignore: cast_nullable_to_non_nullable
              as EngUzbWordEntities?,
      isEngToUzb: null == isEngToUzb
          ? _value.isEngToUzb
          : isEngToUzb // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LocalDictionaryStateCopyWith<$Res>
    implements $LocalDictionaryStateCopyWith<$Res> {
  factory _$$_LocalDictionaryStateCopyWith(_$_LocalDictionaryState value,
          $Res Function(_$_LocalDictionaryState) then) =
      __$$_LocalDictionaryStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      FormzStatus helperStatus,
      List<EngUzbWord> words,
      List<EngUzbWord> results,
      EngUzbWordEntities? selectedWord,
      bool isEngToUzb});
}

/// @nodoc
class __$$_LocalDictionaryStateCopyWithImpl<$Res>
    extends _$LocalDictionaryStateCopyWithImpl<$Res, _$_LocalDictionaryState>
    implements _$$_LocalDictionaryStateCopyWith<$Res> {
  __$$_LocalDictionaryStateCopyWithImpl(_$_LocalDictionaryState _value,
      $Res Function(_$_LocalDictionaryState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? helperStatus = null,
    Object? words = null,
    Object? results = null,
    Object? selectedWord = freezed,
    Object? isEngToUzb = null,
  }) {
    return _then(_$_LocalDictionaryState(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      helperStatus: null == helperStatus
          ? _value.helperStatus
          : helperStatus // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      words: null == words
          ? _value._words
          : words // ignore: cast_nullable_to_non_nullable
              as List<EngUzbWord>,
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<EngUzbWord>,
      selectedWord: freezed == selectedWord
          ? _value.selectedWord
          : selectedWord // ignore: cast_nullable_to_non_nullable
              as EngUzbWordEntities?,
      isEngToUzb: null == isEngToUzb
          ? _value.isEngToUzb
          : isEngToUzb // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_LocalDictionaryState implements _LocalDictionaryState {
  const _$_LocalDictionaryState(
      {this.status = FormzStatus.pure,
      this.helperStatus = FormzStatus.pure,
      final List<EngUzbWord> words = const [],
      final List<EngUzbWord> results = const [],
      this.selectedWord = null,
      this.isEngToUzb = true})
      : _words = words,
        _results = results;

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final FormzStatus helperStatus;
  final List<EngUzbWord> _words;
  @override
  @JsonKey()
  List<EngUzbWord> get words {
    if (_words is EqualUnmodifiableListView) return _words;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_words);
  }

  final List<EngUzbWord> _results;
  @override
  @JsonKey()
  List<EngUzbWord> get results {
    if (_results is EqualUnmodifiableListView) return _results;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  @JsonKey()
  final EngUzbWordEntities? selectedWord;
  @override
  @JsonKey()
  final bool isEngToUzb;

  @override
  String toString() {
    return 'LocalDictionaryState(status: $status, helperStatus: $helperStatus, words: $words, results: $results, selectedWord: $selectedWord, isEngToUzb: $isEngToUzb)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LocalDictionaryState &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.helperStatus, helperStatus) ||
                other.helperStatus == helperStatus) &&
            const DeepCollectionEquality().equals(other._words, _words) &&
            const DeepCollectionEquality().equals(other._results, _results) &&
            (identical(other.selectedWord, selectedWord) ||
                other.selectedWord == selectedWord) &&
            (identical(other.isEngToUzb, isEngToUzb) ||
                other.isEngToUzb == isEngToUzb));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      status,
      helperStatus,
      const DeepCollectionEquality().hash(_words),
      const DeepCollectionEquality().hash(_results),
      selectedWord,
      isEngToUzb);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LocalDictionaryStateCopyWith<_$_LocalDictionaryState> get copyWith =>
      __$$_LocalDictionaryStateCopyWithImpl<_$_LocalDictionaryState>(
          this, _$identity);
}

abstract class _LocalDictionaryState implements LocalDictionaryState {
  const factory _LocalDictionaryState(
      {final FormzStatus status,
      final FormzStatus helperStatus,
      final List<EngUzbWord> words,
      final List<EngUzbWord> results,
      final EngUzbWordEntities? selectedWord,
      final bool isEngToUzb}) = _$_LocalDictionaryState;

  @override
  FormzStatus get status;
  @override
  FormzStatus get helperStatus;
  @override
  List<EngUzbWord> get words;
  @override
  List<EngUzbWord> get results;
  @override
  EngUzbWordEntities? get selectedWord;
  @override
  bool get isEngToUzb;
  @override
  @JsonKey(ignore: true)
  _$$_LocalDictionaryStateCopyWith<_$_LocalDictionaryState> get copyWith =>
      throw _privateConstructorUsedError;
}
