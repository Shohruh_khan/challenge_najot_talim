part of 'local_dictionary_bloc.dart';

@freezed
class LocalDictionaryEvent with _$LocalDictionaryEvent {
  const factory LocalDictionaryEvent.loadEngUzb({
    required String query,
    required ValueChanged<String> onFailure,
  }) = _LoadEngUzb;
  const factory LocalDictionaryEvent.loadUzbEng({
    required String query,
    required ValueChanged<String> onFailure,
  }) = _LoadUzbEng;

  const factory LocalDictionaryEvent.loadDefinition({
    required String query,
    required ValueChanged<String> onFailure,
  }) = _LoadDefinition;

  const factory LocalDictionaryEvent.init({
    // required List<EngUzbWord> allWords,
    required bool isEnglish,
  }) = _Init;
}
