import 'package:bloc/bloc.dart';
import 'package:challenge_nj/core/app_functions.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../core/models/formz/formz_status.dart';
import '../../data/models/dictionary_model.dart';
import '../../domain/entities/eng_uz_dictionary_entity.dart';

part 'local_dictionary_event.dart';
part 'local_dictionary_state.dart';
part 'local_dictionary_bloc.freezed.dart';

class LocalDictionaryBloc
    extends Bloc<LocalDictionaryEvent, LocalDictionaryState> {
  LocalDictionaryBloc() : super(const LocalDictionaryState()) {
    on<_LoadEngUzb>((event, emit) async {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));

      final results = state.words
          .where((element) => element.eng.contains(event.query))
          .toList();
      emit(state.copyWith(results: results));

      // final response = await loadData();
      // response.either(
      //   (failureMessage) {
      //     emit(state.copyWith(status: FormzStatus.submissionFailure));
      //
      //     event.onFailure(failureMessage);
      //   },
      //   (list) {
      //     emit(state.copyWith(words: state.words));
      //   },
      // );
    });
    on<_LoadUzbEng>((event, emit) {
      //TODO
    });
    on<_LoadDefinition>((event, emit) {
      //TODO
    });
    on<_Init>((event, emit) {
      // final words = await AppFunctions.loadData();
      emit(state.copyWith(isEngToUzb: event.isEnglish));
    });
  }
}
