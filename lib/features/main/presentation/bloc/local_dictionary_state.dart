part of 'local_dictionary_bloc.dart';

@freezed
class LocalDictionaryState with _$LocalDictionaryState {
  const factory LocalDictionaryState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default(FormzStatus.pure) FormzStatus helperStatus,
    @Default([]) List<EngUzbWord> words,
    @Default([]) List<EngUzbWord> results,
    @Default(null) EngUzbWordEntities? selectedWord,
    @Default(false) bool isEngToUzb,
  }) = _LocalDictionaryState;
}
