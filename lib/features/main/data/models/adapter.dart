import '../../domain/entities/eng_uz_dictionary_entity.dart';
import 'dictionary_model.dart';

class EngDictionaryMapper {
  static EngUzbWord fromEntity(EngUzbWordEntities entity) {
    return EngUzbWord(
      entity.eng,
      entity.uzb,
    );
  }

  static List<EngUzbWord> fromEntityList(List<EngUzbWordEntities> entities) {
    return entities.map((e) => fromEntity(e)).toList();
  }
}
