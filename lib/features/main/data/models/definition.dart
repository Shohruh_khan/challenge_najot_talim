class DefinitionModel {
  final String Word;
  final String Description;

  DefinitionModel(this.Word, this.Description);
}
