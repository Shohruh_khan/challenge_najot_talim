import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import '../../domain/entities/eng_uz_dictionary_entity.dart';
import '../models/dictionary_model.dart';

abstract class EngDictionaryLocalDataSource {
  Future<List<EngUzbWord>> getEngUzbWords({
    required String eng,
    required String uzb,
    required bool isFav,
    required bool isHistory,
  });
}

class EngDictionaryLocalDataSourceImpl implements EngDictionaryLocalDataSource {
  @override
  Future<List<EngUzbWord>> getEngUzbWords({
    required String eng,
    required String uzb,
    required bool isFav,
    required bool isHistory,
  }) async {
    final db = await openDatabase(
      join(await getDatabasesPath(), 'eng_dictionary.db'),
      version: 1,
    );
    final List<Map<String, dynamic>> maps = await db.query('eng_uzb');

    final list = List.generate(maps.length, (i) {
      return EngUzbWord(
        maps[i]['eng'],
        maps[i]['uzb'],
      );
    });
    return list.where((element) => element.eng.contains(eng)).toList();
  }
}
