import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static Database? _database;

  static Future<Database?> get database async {
    if (_database != null) return _database;

    // Get the path to the database
    String path = join(await getDatabasesPath(), 'eng_dictionary.db');

    // Open the database
    _database = await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
    return _database;
  }

  static Future<void> _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE words (id INTEGER PRIMARY KEY, english TEXT, uzbek TEXT)');
  }

  static Future<List<Map<String, dynamic>>> getWords(String query) async {
    final db = await database;
    return db!
        .query('eng_uzb', where: 'english LIKE ?', whereArgs: ['%$query%']);
  }
}
