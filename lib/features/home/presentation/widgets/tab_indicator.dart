import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../data/models/navbar/navbar.dart';

class TabItemWidget extends StatelessWidget {
  final bool isActive;
  final NavBar item;
  const TabItemWidget({
    this.isActive = false,
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        width: double.maxFinite,
        decoration: const BoxDecoration(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(
              isActive ? item.iconActive : item.icon,
              fit: BoxFit.contain,
            ),
            Text(item.title,
                style: isActive
                    ? Theme.of(context).textTheme.headline1!.copyWith(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        )
                    : Theme.of(context).textTheme.headline4!.copyWith(
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        )),
          ],
        ),
      );
}
