class NavBar {
  final int id;
  final String iconActive;
  final String icon;
  final String title;

  const NavBar({
    required this.id,
    required this.icon,
    required this.title,
    required this.iconActive,
  });
}
