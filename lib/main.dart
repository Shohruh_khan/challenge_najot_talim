import 'dart:io';

import 'package:challenge_nj/features/home/presentation/home.dart';
import 'package:challenge_nj/features/main/presentation/bloc/local_dictionary_bloc.dart';
import 'package:challenge_nj/features/main/presentation/main_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'assets/theme/theme.dart';
import 'core/bloc/show_pop_up/show_pop_up_bloc.dart';
import 'core/data/service_locator.dart';
import 'core/splash/splash_screen.dart';

Future<void> main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  initServiceLocator();
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ShowPopUpBloc>(
          create: (BuildContext context) => ShowPopUpBloc(),
        ),
        BlocProvider<LocalDictionaryBloc>(
          create: (BuildContext context) => LocalDictionaryBloc(),
        ),
      ],
      child: OverlaySupport.global(
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: AppTheme.lightTheme(),
            home: const SplashScreen()),
      ),
    );
  }
}
